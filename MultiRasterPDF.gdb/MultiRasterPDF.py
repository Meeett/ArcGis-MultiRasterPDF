#   MultiRasterPDF.py
#   Author: Mike Wunderlich
#   URL: www.mikewunderlich.de
#   Mail: gitlab@mikewunderlich.de

import arcpy
mxd = arcpy.mapping.MapDocument("CURRENT")
refDF = arcpy.mapping.ListDataFrames(mxd, "*")[0]
arcpy.AddMessage(refDF.name + " Wurde als Referenz gewaehlt")
for df in arcpy.mapping.ListDataFrames(mxd, "*"):
    if refDF.name != df.name:
        try:
            #ElementSquare
            df.elementWidth = refDF.elementWidth
            df.elementHeight = refDF.elementHeight

            #ElementPosition
            df.elementPositionX = refDF.elementPositionX
            df.elementPositionY = refDF.elementPositionY

            #MapOptions
            df.scale = refDF.scale
            df.extent = refDF.extent
            df.spatialReference = refDF.spatialReference
            df.rotation = refDF.rotation

            arcpy.AddMessage(df.name + " wurde an die Referenz angepasst")
        except:
            arcpy.AddError("Es ist ein Fehler aufgetreten")

arcpy.RefreshActiveView()
